const CopyWebpackPlugin = require('copy-webpack-plugin')
const UglifyJsPlugin = require('uglifyjs-webpack-plugin')

// Plugins used for production and development builds
const plugins = [
  new CopyWebpackPlugin(
    [
      { from: 'src/sections/**/*', to: 'sections/', flatten: true },
      { from: 'src/templates/**/*', to: 'templates/', flatten: true },
      { from: 'src/snippets/**/*', to: 'snippets/', flatten: true },
      { from: 'src/config/**/*', to: 'config/', flatten: true },
      { from: 'src/assets/**/*', to: 'assets/', flatten: true },
    ],
    {},
  ),
]

// Plugins used for production only
const productionPlugins = [new UglifyJsPlugin({
  sourceMap: true
})]

module.exports = ({ environment = 'production' }) => {
  if (environment === 'production') {
    return plugins.concat(productionPlugins)
  } else {
    return plugins
  }
}
