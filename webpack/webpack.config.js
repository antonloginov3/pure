const path = require('path');
const getPlugins = require('./plugins');
const getJavascriptRules = require('./rules/javascript');
const getStyleRules = require('./rules/css');

const environment = process.env.NODE_ENV || 'production'
const isDevelopment = (environment === 'development');

module.exports = {
  entry: [
    './src/js/main.js',
    './src/css/main.scss'
  ],
  output: {
    path: path.resolve(__dirname, '../assets'),
    filename: 'main.js',
  },
  devtool: isDevelopment ? 'eval-source-map' : 'none',
  mode: isDevelopment ? 'development' : 'production',
  module: {
    rules:
      [].concat(
        getJavascriptRules({ environment }),
        getStyleRules({ environment }),
      ),
  },
  plugins: getPlugins({ environment }),
  performance: { hints: false },
}
