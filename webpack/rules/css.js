module.exports = ({ environment = 'production' }) => {
  return [{
    test: /\.scss$/,
    use: [
      {
        loader: 'file-loader',
        options: {
          name: '[name].css',
          outputPath: './'
        }
      },
      {
        loader: 'extract-loader'
      },
      {
        loader: 'css-loader',
        options: {
          sourceMap: (environment === 'development'),
          url: false,
        }
      },
      {
        loader: 'postcss-loader'
      },
      {
        loader: 'sass-loader'
      }
    ]
  }]
}
