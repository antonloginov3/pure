module.exports = ({ environment = 'production' }) => {
  return [{
    test: /\.js$/,
    exclude: /node_modules/,
    use: {
      loader: 'babel-loader'
    }
  }]
}
