module.exports = {
	plugins: [
    	require('autoprefixer'),
    	require('postcss-flexbugs-fixes'),
			require('cssnano')({
				zindex: false,
        reduceIdents: false,
			}),
	]
};
